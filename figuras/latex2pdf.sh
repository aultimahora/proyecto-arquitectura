#!/bin/bash 
#pdflatex.sh : simple latex2pdf converter

cd ${0%/*}
FILES=*.tex
for f in $FILES
do
   pdflatex $f 
  # take action on each file. $f store current file name
done
