### **Proyecto Arquitectura de computadores: "Mejora de apuntes del curso"** ###

Integrantes:

- Nicolás Delgado
- Elisa Kauffmann
- Luis Martínez
- Paloma Pérez
- Jonathan Urzúa

###################################################################
*Todos los archivos se encuentran en la carpeta source*

Archivos a trabajar:

- apuntes.tex (Paloma)
- capitulo1.tex: Diseño de circuitos digitales (Elisa)
- capitulo2.tex: Arquitectura lógica de un computador (Jonathan y Elisa)
- capitulo3.tex: Arquitectura física de un computador (Nicolás)
- capitulo4.tex: Entrada/Salida (Paloma)
- capitulo5.tex: Arquitecturas avanzadas (Luis)
################################################################

Instrucciones:

Usted sólo debe modificar el capítulo que escogió para el proyecto. Las modificaciones que haga son independientes a las realizadas por sus compañeros.